package tk.tahri.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.List;

@SpringBootApplication
public class ApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}
}

@Data
@NoArgsConstructor
@Entity
class Article {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String titre;
	private String contenu;
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate date;
	private String auteur;
};


interface ArticleRepository extends JpaRepository<Article,Long>{

}

@RestController
class ArticleWebService {

	private ArticleRepository repository;

	public ArticleWebService(ArticleRepository repository) {
		this.repository = repository;
	}

	@GetMapping(value = "articles" , produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public List<Article> getAllArticles(){
		return repository.findAll();
	}

	@GetMapping("articles/{id}")
	public Article getArticleById(@PathVariable long id){
		return repository.findById(id).orElseThrow(
				()->new ResponseStatusException(HttpStatus.NOT_FOUND,"Article not found")
		);
	}

	@DeleteMapping("articles/{id}")
	public void deleteArticleById(@PathVariable long id){
		repository.deleteById(id);
	}
	@PostMapping("articles")
	public Article saveArticle(@RequestBody Article article){
		return repository.save(article);
	}
	@PutMapping("articles")
	public Article updateArticle(@RequestBody Article article){
		return repository.save(article);
	}
}